FROM node:18 AS build-dist
COPY [".", "/gitlab-ci-matrix-bot/"]
WORKDIR "/gitlab-ci-matrix-bot/"
RUN find /gitlab-ci-matrix-bot/ && \
    yarn install --immutable --immutable-cache --check-cache && \
    yarn build

FROM node:18
ENV NODE_ENV=production
COPY --from=build-dist /gitlab-ci-matrix-bot/dist /gitlab-ci-matrix-bot/dist
COPY --from=build-dist /gitlab-ci-matrix-bot/.yarn /gitlab-ci-matrix-bot/.yarn
COPY --from=build-dist /gitlab-ci-matrix-bot/.yarnrc.yml /gitlab-ci-matrix-bot/.yarnrc.yml
COPY --from=build-dist /gitlab-ci-matrix-bot/yarn.lock /gitlab-ci-matrix-bot/yarn.lock
COPY --from=build-dist /gitlab-ci-matrix-bot/.pnp.cjs /gitlab-ci-matrix-bot/.pnp.cjs
COPY --from=build-dist /gitlab-ci-matrix-bot/package.json /gitlab-ci-matrix-bot/package.json
WORKDIR "/gitlab-ci-matrix-bot/"
RUN yarn workspaces focus --production
CMD ["yarn", "run", "start"]
EXPOSE 8080
