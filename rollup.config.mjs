import typescript from '@rollup/plugin-typescript';
import json from "@rollup/plugin-json";
import ejs from "rollup-plugin-ejs";

export default [
    {
        input: 'src/main.ts',
        output: {
            file: 'dist/main.js',
            sourcemap: true,
            format: 'cjs'
        },
        plugins: [
            json(),
            ejs({
                include: ['**/*.html.ejs'],
                compilerOptions: {
                    strict: true,
                    rmWhitespace: true,
                },
                inlineStyles: false
            }),
            ejs({
                include: ['**/*.text.ejs'],
                compilerOptions: {
                    strict: true,
                    rmWhitespace: false,
                },
                inlineStyles: false
            }),
            typescript(),
        ],
        external: [
            "fs",
            "http",
            "matrix-bot-sdk",
            "path",
            "twig",
            "url",
            "winston",
            "node-cache",
        ]
    }
];
