import {MatrixClient, SimpleFsStorageProvider} from "matrix-bot-sdk";
import {createServer, IncomingMessage, ServerResponse} from "http"
import {URL} from "url";
import * as winston from "winston";
import NodeCache from "node-cache";

// @ts-ignore
import messageHtmlTemplate from "./message.html.ejs";
// @ts-ignore
import messageTextTemplate from "./message.text.ejs";

const logger = winston.createLogger({
    level: process.env.LOG_LEVEL ?? 'debug',
    format: winston.format.simple(),
    transports: [
        new winston.transports.Console({
            level: process.env.LOG_CONSOLE_LEVEL,
            format: winston.format.simple()
        })
    ]
});

if (process.env.LOG_FILE) {
    logger.info(`Write log to ${process.env.LOG_FILE}`);
    logger.add(new winston.transports.File({
        level: process.env.LOG_FILE_LEVEL,
        filename: process.env.LOG_FILE,
    }));
}

const replyToPrevious = "true" === process.env.REPLY_TO_PREVIOUS;

function requireEnv(name: string): string {
    const value = process.env[name];
    if (undefined === value || null === value) {
        throw new Error(`Environment variable ${name} is mandatory`)
    }
    return value;
}

interface PipelineEvent {
    user: {
        id: number;
        name: string;
        username: string;
        avatar_url: string;
        email: string;
    };
    object_attributes: {
        id: number;
        ref: string;
        tag: boolean;
        sha: string;
        status: string;
        stages: string[];
    };
    project: {
        id: number;
        name: string;
        namespace: string;
        path_with_namespace: string;
        web_url: string;
    };
    commit: {
        id: string;
        message: string;
        timestamp: string;
        url: string;
        author: {
            name: string;
            email: string;
        };
    };
    builds: Array<{
        id: number;
        stage: string;
        name: string;
        status: string;
        created_at: string;
        started_at?: string | null;
        finished_at?: string | null;
        allow_failure: boolean;
        user: {
            id: number;
            name: string;
            username: string;
            avatar_url: string;
            email: string;
        };
    }>;
}

const homeserverUrl = requireEnv('HOMESERVER_URL');
const homeserverHost = new URL(homeserverUrl).hostname;
const accessToken = requireEnv('ACCESS_TOKEN');
const secretToken = requireEnv('SECRET_TOKEN');
const storageFilename = process.env.STORAGE_FILE || "gitlab-ci-bot.json";
const httpHost = process.env.HTTP_HOST;
const httpPort = parseInt(process.env.HTTP_PORT || '') || 8080;

logger.info(`Starting Gitlab CI Matrix Bot`);

const cache = new NodeCache({
    stdTTL: 24 * 60 * 60,
    useClones: false,
});

const storage = new SimpleFsStorageProvider(storageFilename);
const matrixClient = new MatrixClient(homeserverUrl, accessToken, storage);
let botUserId = "";

async function findPrivateChatRoomId(matrixUserId: string): Promise<string | null> {
    logger.debug(`Try find private chat with ${matrixUserId}`);

    const joinedRooms = await matrixClient.getJoinedRooms();
    for (const joinedRoom of joinedRooms) {
        if (await validatePrivateChatRoomId(joinedRoom, matrixUserId)) {
            logger.debug(`Private chat with ${matrixUserId} room found: ${joinedRoom}`);
            return joinedRoom;
        }
    }

    return null;
}

async function validatePrivateChatRoomId(roomId: string | null | undefined, matrixUserId: string): Promise<string | null> {
    if (!roomId) {
        return null;
    }

    const roomState = await matrixClient.getRoomState(roomId);
    const members = roomState.filter(roomEvent => "m.room.member" === roomEvent.type);
    const membersString = members.map(member => `${member.user_id}#${member.content.membership}`).join(", ");

    logger.debug(`Validate if room "${roomId}" (members: ${membersString}) is a private chat with ${matrixUserId}`);

    // room must not have a name (otherwise it is not a "private chat" room)
    if (!roomState.find(roomEvent => "m.room.name" === roomEvent.type)) {

        // only 1:1 chats are used
        if (2 === members.length) {

            // one member must be the user
            const botMember = members.find(membership => botUserId === membership.user_id);
            const botMembership = botMember?.content?.membership;
            const userMember = members.find(membership => matrixUserId === membership.user_id);
            const userMembership = userMember?.content?.membership;

            if (("invite" === botMembership || "join" === botMembership) &&
                ("invite" === userMembership || "join" === userMembership)) {
                logger.debug(`Found room "${roomId}" that is a private chat with ${matrixUserId}`);
                return roomId;
            }
        }
    }

    logger.debug(`Room "${roomId}" is NOT a private chat with ${matrixUserId}`);
    return null;
}

async function createPrivateChat(matrixUserId: string): Promise<string> {
    logger.debug(`Create private chat with ${matrixUserId}`);
    const roomId = await matrixClient.createRoom({
        invite: [matrixUserId],
        visibility: "private",
        preset: "private_chat",
        is_direct: true,
        power_level_content_override: {
            ban: 100,
            events_default: 100,
            invite: 100,
            kick: 100,
            redact: 100,
            state_default: 100,
            users_default: 0
        },
        initial_state: [
            {
                type: "m.room.guest_access",
                state_key: "",
                content: {
                    "guest_access": "forbidden"
                }
            }
        ]
    });
    logger.debug(`New private chat with ${matrixUserId} startet, room id: ${roomId}`);
    return roomId;
}

async function matrixUserExists(matrixUserId: string): Promise<boolean> {
    try {
        return !!await matrixClient.getUserProfile(matrixUserId);
    } catch (e) {
        if (404 === e.statusCode) {
            return false;
        }
        throw e;
    }
}

async function handlePipelineEvent(event: PipelineEvent) {
    const gitlabUsername = event.user.username;
    const pipelineId = event.object_attributes.id;
    const ref = event.object_attributes.ref;
    const project = event.project.path_with_namespace;
    const status = event.object_attributes.status;

    const matrixUserId = `@${gitlabUsername}:${homeserverHost}`;

    logger.debug(`Handle pipeline ${pipelineId} event (${ref}@${project} triggered by "${gitlabUsername}")`);

    if (!await matrixUserExists(matrixUserId)) {
        logger.info(`Matrix user ${matrixUserId} for gitlab user "${gitlabUsername}" does not exists`);
        return;
    } else {
        logger.debug(`Propagate ${status} pipeline ${pipelineId} (${ref}@${project}) to ${matrixUserId}`);
    }

    const roomKey = `gitlab_ci_bot_private_chat_${matrixUserId}_room_id`;
    let roomId = storage.readValue(roomKey);
    roomId = await validatePrivateChatRoomId(roomId, matrixUserId);
    if (!roomId) {
        roomId = await findPrivateChatRoomId(matrixUserId) ?? await createPrivateChat(matrixUserId);
        storage.storeValue(roomKey, roomId);
    }

    const msgtype = "failed" == status ? "m.text" : "m.notice";

    const pipelineLastEventKey = `pipeline_${pipelineId}_last_event_id`;
    const lastEventId = replyToPrevious && cache.get<string>(pipelineLastEventKey);

    const message: { [key: string]: any } = {
        "msgtype": msgtype,
        "body": messageTextTemplate(event),
        "format": "org.matrix.custom.html",
        "formatted_body": messageHtmlTemplate(event),
    };
    if (replyToPrevious && lastEventId) {
        message["m.relates_to"] = {
            "m.in_reply_to": {
                "event_id": lastEventId
            }
        };
    }

    const eventId = await matrixClient.sendMessage(roomId, message);
    cache.set(pipelineLastEventKey, eventId);

    logger.debug(`Pipeline ${pipelineId} (${ref}@${project}) event successfully propagated to ${matrixUserId} as ${msgtype}, event id: ${eventId}, in reply to: ${lastEventId}`);
}

async function cleanupRooms() {
    logger.info("Cleanup room memberships");

    const joinedRooms = await matrixClient.getJoinedRooms();

    for (const roomId of joinedRooms) {
        logger.debug(`Cleanup room "${roomId}" membership`);

        const roomState = await matrixClient.getRoomState(roomId);
        const members = roomState.filter(roomEvent => "m.room.member" === roomEvent.type);

        const membersString = members.map(member => `${member.user_id}#${member.content.membership}`).join(", ");

        // leave all rooms if
        // - not exactly two members in the room -> this is not a direct 1:1 chat
        // - any member has left the room
        // - the bot is invited into a room, but has not accepted the invite
        if (members.length != 2 || !members.every(member => {
            const membership = member.content.membership;
            return "invite" === membership || "join" === membership;
        }) || members.some(member => {
            return botUserId == member.user_id && "invite" === member.content.membership;
        })) {
            logger.info(`Dangling room "${roomId}" (members: ${membersString}) will be left`);
            await matrixClient.leaveRoom(roomId)
                .catch(error => {
                    logger.error({
                        message: `Leafing dangling room "${roomId}" (members: ${membersString}) failed: ${error.message ?? error}`,
                        error
                    });
                });
        } else {
            logger.debug(`Remain in 1:1 direct chat room "${roomId}" (members: ${membersString})`);
        }
    }

}

matrixClient.start().then(async () => {
    logger.info(`Matrix Client ${homeserverUrl} started`)
    botUserId = await matrixClient.getUserId();
    logger.info(`Logged in to matrix server as ${botUserId}`);

    await cleanupRooms().then(() => {
        logger.debug("Cleanup rooms cron completed");
    }, (reason) => {
        logger.error(`Cleanup rooms cron failed: ${reason}`);
    });

    // room cleanup interval
    setInterval(function() {
        cleanupRooms().then(() => {
            logger.debug("Cleanup rooms cron completed");
        }, (reason) => {
            logger.error(`Cleanup rooms cron failed: ${reason}`);
        });
    }, 60 * 60 * 1000);
}, async (reason) => {
    logger.error(`Matrix Client failed to start: ${reason}`);
    process.exit(1);
});

matrixClient.on("room.message", (roomId, event) => {
    // mark all messages as read
    if (event.id) {
        matrixClient.sendReadReceipt(roomId, event.id)
            .catch(error => logger.error({
                message: `Mark message event ${event.id} in room ${roomId} as read failed: ${error.message ?? error}`,
                error
            }))
    }
});

matrixClient.on("room.invite", (roomId: string, _inviteEvent: any) => {
    // ignore invites
    matrixClient.leaveRoom(roomId)
        .catch(error => logger.error({
            message: `Reject invite / leafing room ${roomId} failed: ${error.message ?? error}`,
            error
        }));
});

async function requestListener(request: IncomingMessage, response: ServerResponse<IncomingMessage>) {
    if (secretToken !== request.headers['x-gitlab-token']) {
        response.setHeader("content-type", "text/plain");
        response.writeHead(401);
        response.end("Invalid secret token");
    } else if (!request.headers['x-gitlab-event']) {
        response.setHeader("content-type", "text/plain");
        response.writeHead(400);
        response.end("No gitlab event");
    } else {
        const buffers = [];
        for await (const chunk of request) {
            buffers.push(chunk);
        }
        const data = Buffer.concat(buffers).toString();

        const eventType = request.headers['x-gitlab-event'];
        const event = JSON.parse(data);

        response.writeHead(204);
        response.end();

        logger.debug(`Received "${eventType}" gitlab event`);

        if ("Pipeline Hook" === eventType) {
            handlePipelineEvent(event)
                .catch(error => logger.error({
                    message: `Handling gitlab pipeline event ${event.id} failed: ${error.message ?? error}`,
                    event,
                    error
                }));
        }
    }
}

const httpServer = createServer((request, response) => {
    requestListener(request, response).catch(error => {
        logger.log({
            level: 'error',
            message: `Process request failed`,
            error
        });
        response.writeHead(500);
        response.end(JSON.stringify(error));
    })
});

httpServer.listen(httpPort, httpHost, () => {
    logger.info(`Http server ${httpHost ?? '*'}:${httpPort} started!`)
});
